#include "cTransSchwer.h"

double cTransSchwer::preis() {
	double gew_in = 0.0;
	double breite_in = 0.0;
	int aufwand_in = 0;
	double aufwandFaktor = 1.0;
	int anzZuschlag = 0;

	cout << endl << "Neuer Schwertransportauftrag:" << endl;

	cout << "Gewicht eingeben (in kg): ";
	cin >> gew_in;

	cout << "Breite eingeben (in cm): ";
	cin >> breite_in;

	while (aufwand_in < 1 || aufwand_in > 4) {	// Wiederholen bis Werte zw. 1 und 4 sind
		cout << "Begleitungsaufwand (1, 2, 3, 4): ";
		cin >> aufwand_in;
	}

	switch (aufwand_in) {	// determine aufwandFaktor
		case 1:
			aufwandFaktor = 1.0;
			break;
		case 2:
			aufwandFaktor = 1.5;
			break;
		case 3:
			aufwandFaktor = 2.1;
			break;
		case 4:
			aufwandFaktor = 4.2;
			break;
	}

	double tmpBreiteCpy = breite_in;
	while (tmpBreiteCpy > 350) {	// count how often 350 fits into breite_in
		tmpBreiteCpy -= 350;
		anzZuschlag++;
	}

	double preisTmp = 5.4 * (gew_in / 1000);
	double preis = (preisTmp + (preisTmp * 0.1 * anzZuschlag)) * aufwandFaktor;

	cout << "Preis: " << preis << endl;

	return preis;
}