#include "cTransLang.h"

double cTransLang::preis() {
	double gew_in = 0.0;
	double len_in = 0.0;

	cout << endl << "Neuer Transportauftrag nach Laenge:" << endl;

	cout << "Gewicht eingeben (in kg): ";
	cin >> gew_in;

	cout << "Laenge eingeben (in m): ";
	cin >> len_in;

	double laengenzuschlag = 0.0;

	if (len_in >= 5.0)
		laengenzuschlag = len_in * 0.1;		// >= 5m: +10%
	else if (len_in >= 9.0)
		laengenzuschlag = len_in * 0.2;		// >= 9m: +20%
	else if (len_in >= 14.0)
		laengenzuschlag = len_in * 0.4;		// >= 14m: +40%

	double preis = 1.28 * (gew_in / 1000) + laengenzuschlag;
	cout << "Preis: " << preis << endl;

	return preis;
}