#pragma once

#include <iostream>

using namespace std;

class cTransport	// abstract class
{
private:
	double distanz;
public:
	cTransport(double = 100.0);
	double getDistance() const;
	virtual double preis() = 0;
};
