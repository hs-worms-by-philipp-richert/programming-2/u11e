#include "cTransGew.h"
#include "cTransVol.h"
#include "cTransLang.h"
#include "cTransEil.h"
#include "cTransSchwer.h"

int main() {
	double gesamtpreis = 0.0;
	cTransport* transportliste[] = {
		new cTransGew,
		new cTransVol,
		new cTransLang,
		new cTransEil,
		new cTransSchwer,
		new cTransVol,
		new cTransGew,
		new cTransSchwer
	};

	for (cTransport* ct : transportliste) {
		gesamtpreis += ct->preis() * ct->getDistance();
	}

	cout << endl << "=> Gesamtpreis: " << gesamtpreis << " EUR" << endl;

	// delete instances
	for (cTransport* ct : transportliste)
		delete ct;

	return EXIT_SUCCESS;
}