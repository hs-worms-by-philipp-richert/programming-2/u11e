#include "cTransGew.h"

double cTransGew::preis() {
	double gew_in = 0.0;

	cout << endl << "Neuer Transportauftrag nach Gewicht:" << endl;
	
	cout << "Gewicht eingeben (in kg): ";
	cin >> gew_in;

	double preis = 1.65 * (gew_in / 1000);	// 1,65 EUR pro Tonne
	cout << "Preis: " << preis << endl;

	return preis;
}