#include "cTransVol.h"

double cTransVol::preis() {
	double vol_in = 0.0;

	cout << endl << "Neuer Transportauftrag nach Volumen:" << endl;

	cout << "Volumen eingeben (in m�): ";
	cin >> vol_in;

	double preis = 1.09 * vol_in;	// 1,09 EUR pro Kubikmeter
	cout << "Preis: " << preis << endl;

	return preis;
}