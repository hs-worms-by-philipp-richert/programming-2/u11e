#include "cTransEil.h"

double cTransEil::preis() {
	double gew_in = 0.0;
	double tagFaktor = 1.0;
	int tag_in = 0;
	int prio_in = 0;

	cout << endl << "Neuer eilender Transportauftrag:" << endl;

	cout << "Gewicht eingeben (in kg): ";
	cin >> gew_in;

	while (tag_in < 1 || tag_in > 7) {		// Wiederholen bis Werte zw. 1 und 7 sind
		cout << "Wochentag eingeben (1-7): ";
		cin >> tag_in;
	}
	
	while (prio_in < 1 || prio_in > 3) {	// Wiederholen bis Werte zw. 1 und 3 sind
		cout << "Eiligkeit eingeben (1, 2, 3): ";
		cin >> prio_in;
	}

	switch (tag_in) {	// Mo-Do: Faktor = 1.0
		case 5:			// Fr-Sa: Faktor = 1.8
		case 6:
			tagFaktor = 1.8;
			break;
		case 7:			// So:	  Faktor = 2.5
			tagFaktor = 2.5;
			break;
	}

	double preis = 2.75 * prio_in * tagFaktor;
	cout << "Preis: " << preis << " EUR" << endl;

	return preis;
}